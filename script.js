let courses = [];

const addCourse = (course) => {
	courses.push({
		id: course.id,
		name: course.name,
		description: course.description,
		prices: course.price,
		isActive: true
	})

	alert(`You have created ${course.name}. Its price is ${course.price}`)
}


const getSingleCourse = (courseId) => {
	let index = courses.findIndex(course => course.id == courseId);
	console.log(courses[index]);
}

const getAllCourses = () => {
	console.log(courses)
}

const archieveCourse = (courseId) => {
	let index = courses.findIndex(course => course.id == courseId);
	courses[index].isActive = !courses[index].isActive

	console.log(courses[index])
}


const deleteCourse = (courseId) => {
	let index = courses.findIndex(course => course.id == courseId);
	courses.splice(index, 1);
	console.log(courses);
}

const getAllActiveCourse = () => {
	let result = courses.filter(course => course.isActive === true)
	console.log(result)
}

